package com.example.betwiki;

import android.graphics.Bitmap;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;
import moxy.viewstate.strategy.alias.Skip;

public interface MainViewInterface extends MvpView {
    @AddToEnd
    void showMessage(String message);

    @AddToEnd
    void openFaq(String text);
    @AddToEnd
    void setBackground(Bitmap picture);

}
