package com.example.betwiki.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;

import com.example.betwiki.MainPresenter;
import com.example.betwiki.MainViewInterface;
import com.example.betwiki.R;
import com.example.betwiki.data.Page;
import com.example.betwiki.data.Variables;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class MainMenu extends MvpAppCompatFragment implements MainViewInterface {
    View view;

    @InjectPresenter
    MainPresenter presenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getActivity().finishAffinity();
                System.exit(0);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_menu_layout, parent, false);

        Button first = view.findViewById(R.id.menu_1);
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Grid grid = new Grid();
                Bundle bundle = new Bundle();
                bundle.putInt("flag", Variables.TYPES_OF_GAMBLING);
                grid.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerView, grid).addToBackStack(null).commit();
            }
        });
        Button second = view.findViewById(R.id.menu_2);
        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Grid grid = new Grid();
                Bundle bundle = new Bundle();
                bundle.putInt("flag", Variables.STRATEGIES_OF_GAMBLING);
                grid.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerView, grid).addToBackStack(null).commit();
            }
        });
        Button faqButton = view.findViewById(R.id.menu_3);
        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showFaq();
            }
        });

        return view;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openFaq(String text) {
        Output faq = new Output(new Page(getString(R.string.faq), text));
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.fragmentContainerView, faq).addToBackStack(null).commit();
    }

    @Override
    public void setBackground(Bitmap pictire) {
        getActivity().getWindow().getDecorView().findViewById(R.id.base_layout).setBackground(new BitmapDrawable(getContext().getResources(), pictire));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportFragmentManager().beginTransaction().
                remove(this).commit();
    }
}
