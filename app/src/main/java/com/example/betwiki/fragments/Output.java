package com.example.betwiki.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.betwiki.R;
import com.example.betwiki.data.Page;

public class Output extends Fragment {
    private View view;
    private String header = "", text = "";

    public Output(Page info) {
        header = info.header;
        text = info.text;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getActivity().getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.enter_animation,R.anim.exit_anim).
                        replace(R.id.fragmentContainerView, new MainMenu()).addToBackStack(null).commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.output_layout, parent, false);

        TextView head = view.findViewById(R.id.output_header);
        head.setText(header);
        TextView info = view.findViewById(R.id.output_textView);
        info.setText(text);

        ImageButton backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.enter_animation,R.anim.exit_anim).
                        replace(R.id.fragmentContainerView, new MainMenu()).addToBackStack(null).commit();
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportFragmentManager().beginTransaction().
                remove(this).commit();
    }

}
