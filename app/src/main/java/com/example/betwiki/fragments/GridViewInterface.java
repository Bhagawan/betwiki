package com.example.betwiki.fragments;

import com.example.betwiki.data.Page;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface GridViewInterface extends MvpView {
    @AddToEnd
    void updateGrid(ArrayList<Page> dataArray);
    @AddToEnd
    void tost(String string);
    @AddToEnd
    void show(Page page);

}
