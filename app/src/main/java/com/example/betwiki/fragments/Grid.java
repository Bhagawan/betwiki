package com.example.betwiki.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.betwiki.R;
import com.example.betwiki.data.Page;
import com.example.betwiki.utils.Grid_Manager;

import java.util.ArrayList;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class Grid extends MvpAppCompatFragment implements GridViewInterface{
    View view;
    int type;

    @InjectPresenter
    GridPresenter gridPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragmentContainerView, new MainMenu()).addToBackStack(null).commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.grid_layout, parent, false);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            type = bundle.getInt("flag");
            gridPresenter.fillGrid(type);
        }
        return view;
    }

    @Override
    public void updateGrid(ArrayList<Page> dataArray) {
        RecyclerView recyclerView = view.findViewById(R.id.recucler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        Grid_Manager adapter = new Grid_Manager(dataArray);
        adapter.setClickListener(new Grid_Manager.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gridPresenter.showPiece(position, type);
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gridPresenter.detachView(this);
        getActivity().getSupportFragmentManager().beginTransaction().
                remove(this).commit();
    }

    @Override
    public void tost(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void show(Page page) {
        Output output = new Output(page);
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.fragmentContainerView, output).addToBackStack(null).commit();
    }
}
