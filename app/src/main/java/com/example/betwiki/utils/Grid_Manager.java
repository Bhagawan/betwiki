package com.example.betwiki.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.betwiki.R;
import com.example.betwiki.data.Page;

import java.util.ArrayList;

public class Grid_Manager extends RecyclerView.Adapter<Grid_Manager.ViewHolder>{
    private ArrayList<Page> array;
    private ItemClickListener mClickListener;

    public Grid_Manager(ArrayList<Page> array) {
        this.array = array;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView header;
        //ConstraintLayout parent;
        public ViewHolder(View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.header_textView);
            //parent = itemView.findViewById(R.id.constraint_Layout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public Grid_Manager.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.glid_piece_layout, parent, false);
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.one_piece, parent, false);
        return new Grid_Manager.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Grid_Manager.ViewHolder holder, int position) {
        Page page = array.get(position);
        holder.header.setText(page.header);
    }

    @Override
    public int getItemCount() {
        if(array.isEmpty()) return 0;
        else return array.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
