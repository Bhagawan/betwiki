package com.example.betwiki.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Wiki {
    public static ArrayList<Page> kinds = new ArrayList<>();
    public static ArrayList<Page> strategies = new ArrayList<>();
    public static String help;

    public static void addGambleKinds(JSONArray array) throws JSONException {
        for(int i = 0; i < array.length(); i++) {
            kinds.add(new Page((JSONObject) array.get(i)));
        }
    }
    public static void addGambleStrategies(JSONArray array) throws JSONException {
        for(int i = 0; i < array.length(); i++) {
            strategies.add(new Page((JSONObject) array.get(i)));
        }
    }
    public static void addHelp(String page) {
        help = page;
    }

}
