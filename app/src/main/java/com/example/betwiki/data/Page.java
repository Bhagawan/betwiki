package com.example.betwiki.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Page {
    public String header, text;

    public Page(JSONObject object) throws JSONException {
        header = object.getString("name");
        text = object.getString("text");
    }

    public Page (String s, String b) {
        header = s;
        text = b;
    }

}
