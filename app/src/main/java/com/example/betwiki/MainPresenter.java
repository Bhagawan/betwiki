package com.example.betwiki;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;

import com.example.betwiki.data.Wiki;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainViewInterface> {

    public MainPresenter() {}

    @Override
    public void onFirstViewAttach() {
        getBackground("http://116.202.108.46/Bet_Wiki/pic/background.png");
    }

    public void showFaq() {
        if(Wiki.help != null) {
            getViewState().openFaq(Wiki.help);
        }
        else networkCall();
    }

    private void networkCall() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject faq = null;

                try {
                    URL url = new URL("http://116.202.108.46/Bet_Wiki/FAQ.json");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    InputStream input = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder string = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        string.append(line).append('\n');
                    }
                    int rr;
                    faq = new JSONObject(string.toString());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                JSONObject finalFaq = faq;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(finalFaq != null) {
                            try {
                                String text = finalFaq.getString("text");
                                Wiki.addHelp(text);
                                getViewState().openFaq(text);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });
    }

    private void getBackground(String string) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        final Bitmap[] bitmap = new Bitmap[1];
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(string);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                     bitmap[0] = BitmapFactory.decodeStream(input);

                }catch (IOException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(bitmap[0] != null) {
                            getViewState().setBackground(bitmap[0]);
                        }
                    }
                });
            }
        });
    }

}
